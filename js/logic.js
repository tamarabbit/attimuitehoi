
window.onload = function() {
 
    const imgSrc = new Array('src/img/gu.png','src/img/choki.png','src/img/pa.png');
    const attiSrc = new Array('src/img/ue.png','src/img/migi.png','src/img/sita.png','src/img/hidari.png');
    const jankenImg = document.getElementById('janImage');
    const guBtn = document.getElementById('gu');
    const chokiBtn = document.getElementById('choki');
    const paBtn = document.getElementById('pa');

    const migiBtn = document.getElementById('migi');
    const hidariBtn = document.getElementById('hidari');
    const ueBtn = document.getElementById('ue');
    const sitaBtn = document.getElementById('sita');
    const restart = document.getElementById('reload');
    
    let random = Math.floor(Math.random() * imgSrc.length);
    let ram = Math.floor(Math.random() * attiSrc.length);
    
    let myHand = 0; // 自分のじゃんけんの手
    let myDirection = 0; // 自分のあっち向いてホイの手
    let cpu = 0; // CPUのあっち向いてホイの手

    let fight = 0; // じゃんけん勝ち負け
    let game_variable = 0; // この試合の勝ち負け

    cpu = ram;

    // じゃんけん勝ち負け判定
    function jankenPon(random,myHand,fight){
        
        document.getElementById('aiko').innerHTML = "ぽん！";

        if(random == 0 && myHand == 1 || random == 1 && myHand == 2 || random == 2 && myHand == 0 ){
            // CPU勝ちの処理
            
            fight = 0;
            syouhai(fight);
            jankenEnd();
            attimuite();
        }
        if(random == 0 && myHand == 2 || random == 1 && myHand ==0 || random == 2 && myHand == 1 ){
            // CPUが負けの処理
            
            fight = 1;
            syouhai(fight);
            jankenEnd();
            attimuite();
        }
        else if(random == myHand){
            // あいこの処理
            document.getElementById('aiko').innerHTML = "あいこです、もう一度";
            setTimeout("location.reload();", 2000);
        }

        return random,myHand,fight;
        
    }


    guBtn.onclick = function(){
    jankenImg.src = imgSrc[random];
    myHand = 0;
    jankenPon(random,myHand,fight);
    }

    chokiBtn.onclick = function(){
    jankenImg.src = imgSrc[random];
    myHand = 1;
    jankenPon(random,myHand,fight);
    }

    paBtn.onclick = function(){
    jankenImg.src = imgSrc[random];
    myHand = 2;
    jankenPon(random,myHand,fight);
    
    }

    // ここからあっち向いてホイ

    ueBtn.onclick = function(){
        
        myDirection = 0;
        hoi();
        randomImage(ram,jankenImg,attiSrc);
        gameLogic(fight,myDirection,cpu,jankenImg,game_variable);

    }

    migiBtn.onclick = function(){
        
        myDirection = 1;
        hoi();
        randomImage(ram,jankenImg,attiSrc);
        gameLogic(fight,myDirection,cpu,jankenImg,game_variable);

    }

    hidariBtn.onclick = function(){
        
        myDirection = 3;
        hoi();
        randomImage(ram,jankenImg,attiSrc);
        gameLogic(fight,myDirection,cpu,jankenImg,game_variable);
        
    }

    sitaBtn.onclick = function(){
        
        myDirection = 2;
        hoi();
        randomImage(ram,jankenImg,attiSrc);
        gameLogic(fight,myDirection,cpu,jankenImg,game_variable);

    }

    restart.onclick = function(){
    location.reload();
    }



}

function changeWin(jankenImg){
    jankenImg.src = "src/img/win.png";
    return jankenImg;
}

function changeLoser(jankenImg){
    jankenImg.src = "src/img/lose.png";
    return jankenImg;
}


function gameLogic(fight,myDirection,cpu,jankenImg,game_variable){

    if( fight == 0 && myDirection == cpu ){
        // あなたの勝ち
        game_variable = 1;
        changeWin(jankenImg);
        gameset(game_variable,jankenImg);

    }else if( fight == 1 && myDirection == cpu ){
        // あなたの負け
        game_variable = 0;
        changeLoser(jankenImg);
        gameset(game_variable,jankenImg);
        
    }else{
        setTimeout("location.reload();", 3000);
        // もういっかい
    }

    return fight,myDirection,cpu,game_variable;

}


function randomImage(ram,jankenImg,attiSrc){

    jankenImg.src = attiSrc[ram];
    return ram;   
}

function syouhai(fight){
    
    if( fight == 1 ){

        document.getElementById('syouhai').innerHTML = "じゃんけんはあなたの勝ちです";
        document.getElementById('syouhai').style.display = "inline-block";
        
    }else{
        document.getElementById('syouhai').innerHTML = "じゃんけんはあなたの負けです";
        document.getElementById('syouhai').style.display = "inline-block";
    }

    return fight;
    
}


function hoi(){
    document.getElementById('aiko').innerHTML = "ほい";
    document.getElementById('syouhai').style.display = "none";

}


function attimuite(){
    document.getElementById('aiko').innerHTML = "あっち向いて";
}



function gameset(game_variable,jankenImg){
    // 最後に実行

    if(game_variable == 1){
        
        
        document.getElementById('syouhai').innerHTML = "あなたの負けです";
        document.getElementById('syouhai').style.display = "inline-block";
        document.getElementById('aiko').style.display = "none";

    }else{

        document.getElementById('syouhai').innerHTML = "あなたの勝ちです";
        document.getElementById('syouhai').style.display = "inline-block";
        document.getElementById('aiko').style.display = "none";
        
    }

    return game_variable,jankenImg;

}


function jankenEnd(){
    
    document.getElementById('hidari').style.display = "inline-block";
    document.getElementById('migi').style.display = "inline-block";
    document.getElementById('sita').style.display = "inline-block";
    document.getElementById('ue').style.display = "inline-block";
    document.getElementById('gu').style.display = "none";
    document.getElementById('choki').style.display = "none";
    document.getElementById('pa').style.display = "none";
}
